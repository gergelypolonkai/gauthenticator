namespace GAuthenticator {
    [GtkTemplate (ui = "/eu/polonkai/gergely/gauthenticator/gauth-window.ui")]
    class Window : Gtk.ApplicationWindow {
        [GtkChild]
        private Gtk.ProgressBar countdown;

        [GtkChild]
        private Gtk.ListBox auth_list;

        private List<OTPRow> rows = null;

        private void update_totps(bool first = false) {
            foreach (var row in rows) {
                row.update(first);
            }
        }

        private void check_update_totps() {
            var now = new GLib.DateTime.now_utc();
            var remaining = 30.0 - (now.get_seconds() % 30.0);

            if (remaining <= 1.0 / 24.0) {
                update_totps();
            }
        }

        private bool update_countdown() {
            var timestamp = new GLib.DateTime.now_local();
            var current_seconds = timestamp.get_seconds();
            var start_seconds = (current_seconds > 30.0) ? 30.0 : 0.0;

            countdown.fraction = 1.0 - ((current_seconds - start_seconds) / 30.0);

            check_update_totps();

            return true;
        }

        public Window(GAuthenticator.App app) {
            Object(application: app);

            // Roughly 1/24 second, for a smooth(ish) update
            GLib.Timeout.add(41, update_countdown);

            update_totps(true);
        }
    }
}
