namespace GAuthenticator {
    class App : Gtk.Application {
    	private GAuthenticator.Window? window = null;

    	protected override void activate() {
    		if (window == null) {
    			window = new GAuthenticator.Window(this);
    			window.show_all();
    		}

    		window.present();
    	}
    }
}
