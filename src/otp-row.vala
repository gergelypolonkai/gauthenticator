namespace GAuthenticator {
    [GtkTemplate (ui = "/eu/polonkai/gergely/gauthenticator/otp-row.ui")]
    class OTPRow : Gtk.Grid {
        [GtkChild]
        private Gtk.Label code;

        [GtkChild]
        private Gtk.Label provider_name;

        [GtkChild]
        private Gtk.Label account_name;

        public string secret {get; set;}
        public string provider {get; set;}
        public string account {get; set;}

        private OTP.TOTP generator;

        public OTPRow(string secret, string provider, string account) {
            Object(secret: secret, provider: provider, account: account);

            generator = new OTP.TOTP(secret, 6, GLib.ChecksumType.SHA1, 30);

            provider_name.set_text(provider);
            account_name.set_text(account);
        }

        public void update(bool first = false) {
            uint64 val;

            // FIXME: The first run is OK, but the rest is behind one cycle
            if (first) {
                val = generator.now();
            } else {
                var dt = new GLib.DateTime.now_utc();
                val = generator.at(dt, 1);
            }

            code.set_text("%06lu".printf((ulong)val));
        }
    }
}
